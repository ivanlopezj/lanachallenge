FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /lana
COPY requirements.txt /lana/
RUN pip install -r requirements.txt
COPY . /lana/